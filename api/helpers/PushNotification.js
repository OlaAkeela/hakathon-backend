/**
 * Created by yshurrab on 03/12/16.
 */
module.exports = {

  send_push_notification: function(params, callback) {
        // push_notifications._send_apn_push_notification(params, callback);
        PushNotifications._send_fcm_push_notification(params, callback);
  },
  _send_apn_push_notification: function(params, callback) {
    var apn = require("apn");
    var options = {
      key: config.APPLE.KEY,
      cert: config.APPLE.CERT,
      passphrase: config.APPLE.PASSPHRASE,
      production: config.APPLE.PRODUCTION
    };

    var connection = new apn.Connection(options);
    var notification = new apn.Notification();
    var device = new apn.Device(params.token);

    notification.alert = params.message;
    notification.payload = params.payload;
    notification.silent = false;
    notification.sound = "dong.aiff";
    notification.device = device;
    connection.sendNotification(notification);
  },

  _send_fcm_push_notification: function(params, callback) {
    var FCM = require('fcm-node');
    var apiKey = ' AIzaSyAiyCQws0HeJytGiYMtePMM1Q6_CYPnpYg';
    var fcm = new FCM(apiKey);
    params.payload = {
      title: params.type,
      body: params.post_id,
      body: params.title,
      body: params.subtitle
    };
    var message = {
      to: "",
      collapse_key: 'demo',
      notification: {
        title: params.title,
        body: params.subtitle
      },
      data: {
        delayWhileIdle: true,
        timeToLive: 3,
        key1: params.subtitle,
        payload: params.payload
      }
    };

    fcm.send(message, function(err, response) {
      if (err) {
        callback(false, err);
      } else {
        callback(true, response);
      }
    });
  }
};
